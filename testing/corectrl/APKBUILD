# Contributor: Leo <thinkabit.ukim@gmail.com>
# Maintainer:
pkgname=corectrl
pkgver=1.3.2
pkgrel=0
pkgdesc="Control your hardware with application profiles"
url="https://gitlab.com/corectrl/corectrl"
# fails to build
arch="all !armhf !armv7 !ppc64le !x86 !s390x"
license="GPL-3.0-only"
depends="
	hwdata
	procps
	qt5-qtquickcontrols2
	qt5-qtsvg
	qt5-qtxmlpatterns
	"
makedepends="
	botan-dev
	cmake
	fmt-dev
	polkit-dev
	pugixml-dev
	qt5-qtbase-dev
	qt5-qtcharts-dev
	qt5-qtquickcontrols2-dev
	qt5-qtsvg-dev
	qt5-qttools-dev
	quazip-dev
	samurai
	"
checkdepends="catch2"
source="https://gitlab.com/corectrl/corectrl/-/archive/v$pkgver/corectrl-v$pkgver.tar.gz
	"
builddir="$srcdir/$pkgname-v$pkgver"
options="!check" # one version comparison test segfaults

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake -B build -G Ninja \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=None \
		-DBUILD_TESTING="$(want_check && echo ON || echo OFF)" \
		$CMAKE_CROSSOPTS
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
7928483acc34d9344106af279cea4d18cd81211419392d00dbcf5549b771e0ba0b1a10f1caad86df379d178d3d71b57c3de5695ef6dcf901bec66bc23348b5e1  corectrl-v1.3.2.tar.gz
"
